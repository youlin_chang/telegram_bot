<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use App\Libraries\Telegram;
use App\Services\StockService;

class TelegramController extends BaseController
{
    public function index()
    {
        $info = StockService::getInstance()->getTodayInfo();

        //送圖片
        // Telegram::getInstance()->sendPhoto(storage_path('app/public/1536393687823.jpg'));
        //送訊息
        Telegram::getInstance()->sendMessage(
            "股票代碼:{$info['list'][0]->stockCode}\n".
            "股票名稱:{$info['list'][0]->stockName}\n".
            "本日交易量:{$info['list'][0]->dealStockNum}\n".
            "總交易金額:{$info['list'][0]->dealStockMoney}\n".
            "開盤價:{$info['list'][0]->openPrice}\n".
            "最高價:{$info['list'][0]->highestPrice}\n".
            "最低價:{$info['list'][0]->lowestPrice}\n".
            "收盤價:{$info['list'][0]->closePrice}\n".
            "漲跌幅:{$info['list'][0]->priceDifference}\n");
    }
}