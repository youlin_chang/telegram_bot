<?php

namespace App\Presenters;

class TodayStockPresenter {

    public $stockCode;

    public $stockName;

    public $dealStockNum;

    public $dealStockMoney;

    public $openPrice;

    public $highestPrice;

    public $lowestPrice;

    public $closePrice;

    public $priceDifference;

    public $dealNum;
}