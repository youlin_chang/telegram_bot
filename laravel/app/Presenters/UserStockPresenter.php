<?php

namespace App\Presenters;

class UserStockPresenter extends Presenter
{
    public function property() :array
    {
        return [
            'userId',
            'stockCode'
        ];
    }
}