<?php

namespace App\Presenters;

abstract class Presenter
{
    public function __construct()
    {   
        $property = $this->property();

        foreach($property as $param => $value) {
            $this->$param = $value;
        }
    }

    protected function property()
    {
        throw new \Exception('Prensenter No Set Property');
    }
}