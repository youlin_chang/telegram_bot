<?php

namespace App\Presenters;

class TestPresenter extends Presenter
{
    public function property() :array
    {
        return [
            'test' => 1,
            'test2'
        ];
    }
}