<?php

namespace App\Presenters;

class RealTimeStockPresenter
{
    public $stockCode;

    public $stockName;

    public $nowDealPrice;

    public $nowDealNum;

    public $totalDealNum;

    public $openPrice;

    public $highestPrice;

    public $lowestPrice;

    public $yesterdayPrice;

    public $nowPrice;

    public $priceDifference;
}