<?php

namespace App\Services;

use App\Presenters\RealTimeStockPresenter;
use App\Presenters\TodayStockPresenter;
use App\Helpers\RequestHelper;
use App\Helpers\CommonHelper;

class StockService
{
    private static $instance;

    public static function getInstance()
    {
        if(self::$instance == null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getTodayInfo()
    {
        $apiRequest = new RequestHelper();

        $url = 'https://www.twse.com.tw/exchangeReport/STOCK_DAY_ALL?response=open_dat';

        $result = $apiRequest->get($url);

        if($result['code'] != 100) {
            return [];
        }

        $output = [
            'date' => $result['data']->date,
            'title' => $result['data']->title,
            'list' => []
        ];

        foreach($result['data']->data as $stockData) {
            $todayStock = new TodayStockPresenter();
            $todayStock->stockCode = $stockData[0];
            $todayStock->stockName = $stockData[1];
            $todayStock->dealStockNum = $stockData[2];
            $todayStock->dealStockMoney = $stockData[3];
            $todayStock->openPrice = $stockData[4];
            $todayStock->highestPrice = $stockData[5];
            $todayStock->lowestPrice = $stockData[6];
            $todayStock->closePrice = $stockData[7];
            $todayStock->priceDifference = $stockData[8];
            $todayStock->dealNum = $stockData[9];
            $output['list'][] = $todayStock;
        }

        return $output;
    }

    /*
    *   'c' => '股票代號', 
    *   'n' => '公司簡稱', 
    *   'z' => '當盤成交價', 
    *   'tv' => '當盤成交量', 
    *   'v' => '累積成交量', 
    *   'o' => '開盤價', 
    *   'h' => '最高價', 
    *   'l' => '最低價', 
    *   'y' => '昨天收盤價',
    *   'a' => '賣出價格'
    */
    public function getRealTimeInfo(string $stockCode)
    {
        $stockColumn = ['c', 'n', 'z', 'tv', 'v', 'o', 'h', 'l', 'y','a'];

        $apiRequest = new RequestHelper();

        $url = "https://mis.twse.com.tw/stock/api/getStockInfo.jsp?ex_ch=tse_{$stockCode}.tw";

        $result = $apiRequest->get($url);

        if($result['code'] != 100) {
            return [];
        }

        $info = $result['data']->msgArray[0];

        //擷取今天價格
        $nowPrice = substr($info->a, 0, 6);
        //漲跌百分比
        $priceDifference = round(($nowPrice - $info->y)/$info->y * 100, 2);

        
        $realTimeStock = new RealTimeStockPresenter();
        $realTimeStock->stockCode = $info->c;
        $realTimeStock->stockName = $info->n;
        $realTimeStock->nowDealPrice = $info->z;
        $realTimeStock->nowDealNum = $info->tv;
        $realTimeStock->totalDealNum = $info->v;
        $realTimeStock->openPrice = $info->o;
        $realTimeStock->highestPrice = $info->h;
        $realTimeStock->lowestPrice = $info->l;
        $realTimeStock->yesterdayPrice = $info->y;
        $realTimeStock->nowPrice = $nowPrice;
        $realTimeStock->priceDifference = CommonHelper::getStockPriceDifference($nowPrice, $info->y);

        $output = [
            'info' => $realTimeStock,
            'dateTime' => date('Y-m-d').' '.$result['data']->queryTime->sysTime 
        ];

        return $output;
    }
}