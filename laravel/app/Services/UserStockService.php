<?php

namespace App\Services;

use App\Presenters\UserStockPresenter;
use App\Repositories\UserStock\UserStockInterface;
use App;

class UserStockService
{
    protected static $instance;

    protected $repository;

    public function __construct(UserStockInterface $userStock)
    {
        $this->repository = $userStock;
    }

    public static function getInstance()
    {
        if(self::$instance == null) {
            self::$instance = new self(App::make('userStockRepository'));
        }

        return self::$instance;
    }

    public function create($userId, $stockCode)
    {
        if($this->repository->checkUserStockExist($userId, $stockCode)) {
            throw new \ServiceLogicException('Stock Is Exist');
        }

        return $this->repository->create($userId, $stockCode);
    }

    public function getStock($userId)
    {
        $list = $this->repository->getStock($userId);

        $output = [];

        foreach($list as $stock) {
            $userStockPresenter = new UserStockPresenter();
            $userStockPresenter->userId = $stock->user_id;
            $userStockPresenter->stockCode = $stock->stock_code;
            $output[] = $userStockPresenter;
        }

        return $output;
    }
}