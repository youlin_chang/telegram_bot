<?php

namespace App\Services;

use App\Helpers\AuthHelper;
use App\Repositories\User\UserInterface;
use Illuminate\Support\Facades\Hash;
use App;

class UserService
{
    private static $instance;

    protected $repository;

    public function __construct(UserInterface $user)
    {
        $this->repository = $user;
    }

    public static function getInstance()
    {
        if(self::$instance == null) {
            self::$instance = new self(App::make('userRepository'));
        }

        return self::$instance;
    }

    public function checkLogin(string $account,string $password)
    {   
        if(!$this->repository->checkAccountExist($account)) {
            throw new \ServiceLogicException('Account Not Exists');
        }

        $user = $this->repository->getByAccount($account);

        if(!Hash::check($password, $user->password)) {
            throw new \ServiceLogicException('Password Error');
        }

        $token = Hash::make("{$account}@{$password}");
        AuthHelper::setUser($user, $token);

        return $token;
    }

    public function getUserByToken(string $token)
    {
        $user = AuthHelper::getUser($token);
        return $user;
    }
}