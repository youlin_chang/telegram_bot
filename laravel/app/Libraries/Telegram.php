<?php

namespace App\Libraries;

use Telegram\Bot\FileUpload\InputFile;
use Telegram as baseTelegram;

class Telegram
{
    private $bot;

    private $chatId;

    private static $instance;

    protected function __construct()
    {
        $this->bot = baseTelegram::getMe();

        $this->chatId = $this->_getChatGroupId();
    }

    public static function getInstance(): Telegram
    {
        if(self::$instance == null) {
           self::$instance = new self();
        }

        return self::$instance;
    }

    public function getBotId(): int
    {
        return $this->bot->getId();
    }

    public function getBotFirstName(): string
    {
        return $this->bot->getFistName();
    }

    public function getBotUserName(): string
    {
        return $this->bot->getUsername();
    }

    public function sendMessage(string $message)
    {
        baseTelegram::sendMessage([
            'chat_id' => $this->chatId,
            'text' => $message
        ]);

        return $this;
    }

    public function sendPhoto(string $photoUrl, string $filname = '', string $caption = '')
    {
        $filname = empty($filname) ? md5(rand(1, 10000)) : $filname;
        
        baseTelegram::setAsyncRequest(true)
            ->sendPhoto([
                'chat_id' => $this->chatId,
                'photo' => InputFile::create($photoUrl, $filname),
                'caption' => $caption
            ]);

        return $this;
    }

    public function setWebhook(string $callbackUrl)
    {
        baseTelegram::setWebhook(['url' => $callbackUrl]);

        return $this;
    }

    public function getWebhookInfo()
    {
        return baseTelegram::getWebhookUpdates();
    }

    public function getBotChatInfo(): array
    {
        return baseTelegram::getUpdates();
    }

    private function _getChatGroupId(): int
    {
        $botGroup = baseTelegram::getUpdates();

        $chatId = '';
        foreach($botGroup as $item) {
            if(!empty($item['my_chat_member'])) {
                $chatId = $item['my_chat_member']['chat']['type'] == 'group' ? $item['my_chat_member']['chat']['id'] : '';
            }
        }

        return $chatId;
    }
}