<?php

namespace App\Helpers;

use GuzzleHttp\Client;

class RequestHelper
{
    protected $header = ['Content-Type' => 'application/json'];

    protected $response = [
        'code' => 100,
        'message' => 'success',
        'data' => []
    ];

    public function setHeader($key, $value)
    {
        $this->header[$key] = $value;
    }

    public function get($url, $json = true)
    {
        $requestResult = $this->apiRequest('GET', $url);

        $arrayFlag = ($json) ? false : true;

        $requestResult['data'] = (!empty($requestResult['data'])) ? json_decode($requestResult['data'], $arrayFlag) : $requestResult['data'];

        return $requestResult;
    }

    public function post($url, $params = [], $json = true)
    {
        $requestResult = $this->apiRequest('POST', $url, $params);

        $arrayFlag = ($json) ? false : true;

        $requestResult['data'] = (!empty($requestResult['data'])) ? json_decode($requestResult['data'], $arrayFlag) : $requestResult['data'];

        return $requestResult;
    }

    protected function apiRequest($method, $apiUri, $params = [])
    {
        $client = new Client();

        try{
            $request = [];

            if(!empty($this->header)) {
                $request['headers'] = $this->header;
            }

            if(!empty($params) && is_array($params)) {
                $request['body'] = json_encode($params, JSON_UNESCAPED_UNICODE);
            }

            $result = $client->request($method, $apiUri, $request);

            $jsonResult = $result->getBody();

            $this->response = [
                'code' => ($result->getStatusCode() == 200) ? 100 : 400,
                'message' => ($result->getStatusCode() == 200) ? 'succcess' : 'request error',
                'data' => ($result->getStatusCode() == 200) ? $jsonResult : []
            ];
        } catch (\Exception $e) {
            $this->response['code'] = 401;
            $this->response['message'] = $e->getMessage();
        }

        return $this->response;
    }
}