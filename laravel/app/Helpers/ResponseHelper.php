<?php

namespace App\Helpers;

use App\Constants\ResponseCodeConstant;

class ResponseHelper {

    public $result = 1;

    public $code = 0;

    public $message = '';

    public $data = [];
    
    public function response ($result, $code, $message, $data = []) 
    {
        if($this->setResult($result, $code, $message, $data)) {
            return $this->result;
        }

        return FALSE;
    }

    public function responseSuccess ($data = [])
    {
        $this->setResult(1, ResponseCodeConstant::RESPONSE_SUCCESS, 'Success', $data);
        return $this;
    }

    public function responseError ($message = 'System Error', $data = [])
    {
        $this->setResult(0, ResponseCodeConstant::RESPONSE_ERROR, $message, $data);
        return $this;
    }

    public function jsonResponseSuccess($data = [])
    {
        $this->setResult(1, ResponseCodeConstant::RESPONSE_SUCCESS, 'Success', $data);
        return response()->json($this, 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function jsonResponseError($message = 'System Error', $code = ResponseCodeConstant::RESPONSE_ERROR, $data = [])
    {
        $this->setResult(0, $code, $message, $data);
        return response()->json($this, 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function setResult ($result, $code, $message, $data) 
    {

        if(!in_array($result, [0,1])) 
        {
            return FALSE;
        }

        if(!is_integer($code)) 
        {
            return FALSE;
        }

        $this->result = $result;
        $this->code = $code;
        $this->message = $message;
        $this->data = $data;

        return $this;
    }
}