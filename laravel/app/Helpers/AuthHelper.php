<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Cache;

class AuthHelper
{
    public static function setUser($userInfo, $token)
    {
        Cache::put('userInfo-'.$token, json_encode($userInfo), 60*60*60);
    }

    public static function checkUserAuth($token)
    {
        return Cache::has('userInfo-'.$token);
    }

    public static function getUser($token)
    {
        if(Cache::has('userInfo-'.$token)) {
            return json_decode(Cache::get('userInfo-'.$token));
        }

        return [];
    }

    public static function deleteUser($token)
    {
        return Cache::forget('userInfo-'.$token);
    }
}