<?php

namespace App\Helpers;

class CommonHelper
{
    public static function getStockPriceDifference($nowPrice, $yesterdayPrice)
    {
        $priceDifference = round(($nowPrice - $yesterdayPrice)/$yesterdayPrice * 100, 2);

        if($priceDifference > 0) {
            $priceDifference = '+'. $priceDifference;
        } else if($priceDifference < 0) {
            $priceDifference = '-'. $priceDifference;
        }

        return $priceDifference;
    }
}